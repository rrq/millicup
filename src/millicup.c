/**
 * Keep reading cpu.stat usage entries at given rate and print
 * successive differences against last.
 */
#define _GNU_SOURCE         /* See feature_test_macros(7) */

#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

/**
 * Record of cpu.stat reading for a pathname.
 */
typedef struct _Bucket {
    struct _Bucket *next;
    char *path;
    long value;
    double accum;
} Bucket, *BucketP;

/**
 * Extend a head list with a tail list.
 */
static BucketP extend(BucketP head,BucketP tail) {
    if ( tail == 0 ) {
	return head;
    }
    if ( head == 0 ) {
	return tail;
    }
    BucketP last = head;
    while ( last->next ) {
	last = last->next;
    }
    last->next = tail;
    return head;
}

/**
 * Traverse the directory tree at {path} and create Bucket records for
 * all its "cpu.stat" files.
 */
BucketP setup_buckets(const char *path) {
    char *filename = 0;
    BucketP list = 0;
    DIR *dir = opendir( path );
    // Traverse any sub directories recursively, to form its list of
    // Bucket records.
    if ( dir ) {
	struct dirent *entry;
	while (( entry = readdir( dir ))) {
	    if ( entry->d_type != DT_DIR || entry->d_name[0] == '.' ) {
		continue;
	    }
	    char *dirname = 0;
	    if ( asprintf( &dirname, "%s/%s", path, entry->d_name ) > 0 ) {
		BucketP inner = setup_buckets( dirname );
		free( dirname );
		list = extend( list, inner );
	    }
	}
    }
    // Add a record for any "cpu.stat" at {path}
    if ( asprintf( &filename, "%s/cpu.stat", path ) > 0 ) {
	if ( access( filename,  R_OK ) == 0 ) {
	    BucketP b = (BucketP) calloc( 1, sizeof( Bucket ) );
	    b->path = filename;
	    list = extend( list, b );
	} else {
	    free( filename );
	}
    }
    return list;
}

// Terminal control codes
#define TERM_EOLN  "\033[K"
#define TERM_CLEAR "\033[2J"
#define TERM_HOME  "\033[H"

static int TAILW = strlen( "/cpu.stat" );

/**
 * Put the key bit of the pathname bit int the buffer.
 */
static void entryname(char *buffer,char *p) {
    int n = strlen( p ) - TAILW;
    if ( n > 0 ) {
	memcpy( buffer, p, n );
    } else {
	n = 0;
    }
    buffer[ n ] = 0;
}

/**
 * Traverse the Bucket list to make a new reading, and report the
 * "millicup" difference since the last reading. {c0} is the character
 * length of the pathname prefix to leave out of the report; {c1} is
 * the width =f the name column; {w} is the millicup weight, i.e. the
 * factor on the measure difference to make it millicup (average
 * millisecond cpu usage during the period).
 */
void snapshot(BucketP b,int c0,int c1,double w) {
    static char buffer[80];
    long value = 0;
    int fd = open( b->path, O_RDONLY );
    if ( fd < 0 ) {
	perror( b->path );
	exit( 1 ); // This is a bit harsh perhaps?
    }
    int n = read( fd, buffer, 80 );
    if ( ( n > 0 ) && ( sscanf( buffer, "usage_usec %lu\n", &value ) == 1 ) ) {
	// Reuse the buffer to hold the reading name
	entryname( buffer, b->path + c0 );
	w *= value - b->value ;
	b->accum = ( 9.0 * b->accum + w ) / 10.0;
	fprintf( stdout, "%s%*s: % 8.3f % 8.3f\n",
		 TERM_EOLN, c1, buffer, w, b->accum );
	b->value = value;
    }
    close( fd );
}

/**
 * Advance the timespec the given millis, then wait until that time.
 */
void advance_and_wait(struct timespec *time,long millis) {
    long nsec = millis * 1000000 + time->tv_nsec;
    long sec = nsec / 1000000000; // clip subseconds
    nsec -= sec * 1000000000; // clip off full seconds
    time->tv_sec +=  sec;
    time->tv_nsec = nsec;
    struct timespec now;
    if ( clock_gettime( CLOCK_REALTIME, &now ) ) {
        perror( "now" );
	exit( 1 );
    }
    long usec = ( time->tv_sec - now.tv_sec ) * 1000000 +
	( time->tv_nsec - now.tv_nsec ) / 1000;
    if ( usec > 0 ) {
	usleep( (useconds_t) usec );
    }
}

/**
 * Deterimine the macimal path width of bucket pathnames.
 */
static int maxwidth(BucketP b) {
    int n = 0, m = 0;
    for ( ; b; b = b->next ) {
	n = strlen( b->path );
	if ( n > m ) {
	    m = n;
	}
    }
    return m;
}

/**
 * Application main entry.
 * [ period [ path ] ]
 */
int main(int argc, char **argv) {
    static BucketP buckets = 0;
    struct timespec now;
    BucketP b;
    int c0, c1;
    double w;
    char *base = "/sys/fs/cgroup";
    int period = 10000; // default waiting period in ms

    if ( argc > 1 ) {
	// $1 = reporting period in milliseconds
	if ( sscanf( argv[1], "%u", &period ) != 1 ) {
	    fprintf( stderr, "error: $1 = view period in milliseconds\n" );
	    exit( 1 );
	}
	if ( argc > 2 ) {
	    // $2 = base pathname for cpustat
	    base = argv[2];
	}
    }

    setbuf( stdout, 0 ); // No output buffering
    buckets = setup_buckets( base );
    w = 1.0 / period; // permicro worth of 1 ns cpu = millicup
    c0 = strlen( base ) + 1;
    c1 = maxwidth( buckets ) - c0 - TAILW; // The width of name column
    
    if ( clock_gettime( CLOCK_REALTIME, &now ) ) {
	perror( "now" );
	exit( 1 );
    }
    fprintf( stdout, "%s%s", TERM_CLEAR, TERM_HOME );
    // First round with zero factor, to "hide" the initial readings
    for ( b = buckets; b; b = b->next ) {
	snapshot( b, c0, c1, 0 );
    }
    for ( ;; ) {
	advance_and_wait( &now, period );
	fprintf( stdout, "%s", TERM_HOME );
	for ( b = buckets; b; b = b->next ) {
	    snapshot( b, c0, c1, w );
	}
    }
    return 0;
}
